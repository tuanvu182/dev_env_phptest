/**
 * PHPファイルからHTMLファイルを出力します
 * PHPファイルを更新するとブラウザをリロードします
 */

const gulp = require("gulp");
const config = require("../config");
const setting = config.setting;
const $ = require("gulp-load-plugins")(config.loadPlugins);

gulp.task("php2html", () => {});

gulp.task("php", () => {
    //htmlファイルの出力
    if (setting.html.output) {
        gulp.src([
                setting.server.base + "/**/*.php",
                "!" + setting.server.base + "/assets/include"
            ])
            .pipe($.php2html({ baseDir: "./" + setting.server.base }))
            .pipe(gulp.dest(setting.html.dest));
    }

    //ブラウザリロード
    gulp.src(setting.server.base + "/**/*.php").pipe($.browserSync.stream());

    return;
});